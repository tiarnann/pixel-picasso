import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Pixel Picasso")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
